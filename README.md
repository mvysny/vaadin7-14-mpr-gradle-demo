[![Powered By Vaadin on Kotlin](http://vaadinonkotlin.eu/iconography/vok_badge.svg)](http://vaadinonkotlin.eu)
[![pipeline status](https://gitlab.com/mvysny/vaadin14-mpr-gradle-demo/badges/master/pipeline.svg)](https://gitlab.com/mvysny/vaadin14-mpr-gradle-demo/commits/master)

# Vaadin 14 MPR + Vaadin 7 Gradle Demo Project

Template for a simple Kotlin-DSL application that uses the [MPR Multi-Platform Runtime](https://vaadin.com/docs/v14/mpr/Overview.html)
to run Vaadin 7 components in Vaadin 14 app, and it only requires a Servlet 3.1 container to run.
Just clone this repo and start experimenting!

The project demoes:

* A custom Vaadin 7 widget
* Widgetset compilation which includes pieces from Vaadin 7, the MPR and the custom widget. The compilation is done
  using the [Vaadin 7 vaadin-gradle-plugin](https://github.com/johndevs/gradle-vaadin-plugin).
* Vaadin 14 is run in npm+webpack mode (so no compatibility mode), using the
  [Vaadin 14 Gradle Plugin](https://github.com/vaadin/vaadin-gradle-plugin).
* Browserless testing of both Vaadin 14 and Vaadin 7 parts with [Karibu-Testing](https://github.com/mvysny/karibu-testing).
* Gradle
* UI built with Kotlin DSL style using [Karibu-DSL](https://github.com/mvysny/karibu-dsl)

> For Vaadin 8-based example project see [Vaadin 8 + Vaadin 14 MPR Gradle Demo](https://gitlab.com/mvysny/vaadin14-mpr-gradle-demo).

Couple of notes:

* You need to use two Vaadin plugins at the same time: the Vaadin 7 plugin builds the Vaadin7-related bits
  (widgetset/GWT), while the Vaadin 14 plugin builds the Vaadin14-related bits (via npm).
* None of these two Gradle plugins are officially supported by Vaadin:
  * Vaadin 7 plugin was developed by Jon and is long-unmaintained; you might get commercial support by reaching to Jon directly.
  * Vaadin 14 plugin was developed by Martin Vysny and is also unmaintained.
  * The Vaadin company only offers official Gradle support for Vaadin 23+

# Preparing Environment

The Vaadin 14 build requires `node.js` and `npm`. Vaadin Gradle plugin will install those for
you automatically (handy for the CI); alternatively you can install it to your OS:

* Windows: [node.js Download site](https://nodejs.org/en/download/) - use the .msi 64-bit installer
* Linux: `sudo apt install npm`

Also make sure that you have Java 8 (or higher) JDK installed.

# Getting Started

To quickly start the app, just type this into your terminal:

```bash
git clone https://gitlab.com/mvysny/vaadin7-14-mpr-gradle-demo
cd vaadin7-14-mpr-gradle-demo
./gradlew build appRun
```

Gradle will automatically download an embedded servlet container (Jetty) and will run your app in it. Your app will be running on
[http://localhost:8080](http://localhost:8080).

Since the build system is a Gradle file written in Kotlin, we suggest you use [Intellij IDEA](https://www.jetbrains.com/idea/download)
to edit the project files. The Community edition is enough to run the server
via Gretty `./gradlew appRun`. The Ultimate edition will allow you to run the project in Tomcat - this is the recommended
option for a real development.

## Supported Modes

Runs in Vaadin 14 npm mode, using the [Vaadin Gradle Plugin](https://github.com/vaadin/vaadin-gradle-plugin).

Both the [development and production modes](https://vaadin.com/docs/v14/flow/production/tutorial-production-mode-basic.html) are supported.
To prepare for development mode, just run:

```bash
./gradlew clean vaadinPrepareFrontend
```

To build in production mode, just run:

```bash
./gradlew clean build -Pvaadin.productionMode
```

You don't need to have node installed in your CI environment, since Vaadin Gradle
Plugin will download node.js for you automatically:

# Workflow

To compile the entire project in production mode, run `./gradlew -Pvaadin.productionMode`.

To run the application in development mode, run `./gradlew appRun` and open [http://localhost:8080/](http://localhost:8080/).

To produce a deployable production-mode WAR:
- run `./gradlew -Pvaadin.productionMode`
- You will find the WAR file in `build/libs/*.war`
- To revert your environment back to development mode, just run `./gradlew` or `./gradlew vaadinPrepareFrontend`
  (omit the `-Pvaadin.productionMode`) switch.

This will allow you to quickly start the example app and allow you to do some basic modifications.

### Running/Debugging In Intellij Ultimate With Tomcat in Development Mode

* Download and unpack the newest [Tomcat 9](https://tomcat.apache.org/download-90.cgi).
* Open this project in Intellij Ultimate.
* Click "Edit Launch Configurations",
click "Add New Configuration" (the upper-left button which looks like a plus sign + ),
then select Tomcat Server, Local. In the Server tab, the Application Server will be missing,
click the "Configure" button and point Intellij to the Tomcat directory.
  * Still in the launch configuration, in the "Deployment" tab, click the upper-left + button,
    select "Artifact" and select `base-starter-gradle.war (exploded)`.
  * Still in the launch configuration, name the configuration "Tomcat" and click the "Ok" button.

Now make sure Vaadin is configured to be run in development mode - run:

```bash
./gradlew clean vaadinPrepareFrontend assemble
```

* Select the "Tomcat" launch configuration and hit Debug (the green bug button).

If Tomcat fails to start with `Error during artifact deployment. See server log for details.`, please:
* Go and vote for [IDEA-178450](https://youtrack.jetbrains.com/issue/IDEA-178450).
* Then, kill Tomcat by pressing the red square button.
* Then, open the launch configuration, "Deployment", remove the (exploded) war, click + and select `base-starter-gradle.war`.

## Running/Debugging In Intellij Community With Gretty in Development Mode

Make sure Vaadin is configured to be run in development mode - run:

```bash
./gradlew clean vaadinPrepareFrontend assemble
```

In Intellij, open the right Gradle tab, then go into *Tasks* / *gretty*, right-click the
*appRun* task and select Debug. Gretty will now start in debug mode, and will auto-deploy
any changed resource or class.

There are couple of downsides:
* Even if started in Debug mode, debugging your app won't work.
* Pressing the red square "Stop" button will not kill the server and will leave it running.
  Instead, you have to focus the console and press any key - that will kill Gretty cleanly.
* If Gretty says "App already running", there is something running on port 8080. See above
  on how to kill Gretty cleanly.

## Client-Side compilation

The project is using an automatically generated widgetset by default. 
When you add a dependency that needs client-side compilation, the Vaadin Gradle plugin will 
automatically generate it for you. Your own client-side customisations can be added into
package "client".

Debugging client side code  @todo revisit with Gradle
  - run "mvn vaadin:run-codeserver" on a separate console while the application is running
  - activate Super Dev Mode in the debug window of the application

## Developing a theme using the runtime compiler

When developing the theme, Vaadin can be configured to compile the SASS based
theme at runtime in the server. This way you can just modify the scss files in
your IDE and reload the browser to see changes.

To use the runtime compilation, run `./gradlew clean appRun`. Gretty will automatically
pick up changes in theme files and Vaadin will automatically compile the theme on
browser refresh. You will just have to give Gretty some time (one second) to register
the change.

When using the runtime compiler, running the application in the "run" mode 
(rather than in "debug" mode) can speed up consecutive theme compilations
significantly.

It is highly recommended to disable runtime compilation for production WAR files.

# Development with Intellij IDEA Ultimate

The easiest way (and the recommended way) to develop Karibu-DSL-based web applications is to use Intellij IDEA Ultimate.
It includes support for launching your project in any servlet container (Tomcat is recommended)
and allows you to debug the code, modify the code and hot-redeploy the code into the running Tomcat
instance, without having to restart Tomcat.

1. First, download Tomcat and register it into your Intellij IDEA properly: https://www.jetbrains.com/help/idea/2017.1/defining-application-servers-in-intellij-idea.html
2. Then just open this project in Intellij, simply by selecting `File / Open...` and click on the
   `build.gradle` file. When asked, select "Open as Project".
2. You can then create a launch configuration which will launch this example app in Tomcat with Intellij: just
   scroll to the end of this tutorial: https://kotlinlang.org/docs/tutorials/httpservlets.html
3. Start your newly created launch configuration in Debug mode. This way, you can modify the code
   and press `Ctrl+F9` to hot-redeploy the code. This only redeploys java code though, to
   redeploy resources just press `Ctrl+F10` and select "Update classes and resources"

## Dissection of project files

For more info please look into the [karibu-helloworld-application](https://github.com/mvysny/karibu-helloworld-application)
project.

For MPR documentation please see the [MPR Multi-Platform Runtime documentation](https://vaadin.com/docs/v14/mpr/Overview.html).

# More Resources

* The DSL technique is used to allow you to nest your components in a structured code. This is provided by the
  Karibu-DSL library; please visit the [Karibu-DSL home page](https://github.com/mvysny/karibu-dsl) for more information.
* The browserless testing is demonstrated in the [MyUITest.kt](src/test/kotlin/org/test/MyUITest.kt) file.
  Please read [Browserless Web Testing](https://github.com/mvysny/karibu-testing) for more information.
* For more complex example which includes multiple pages, please see the [Karibu-DSL example-v8 app](https://github.com/mvysny/karibu-dsl#quickstart).
* For information on how to connect the UI to the database backend please visit [Vaadin-on-Kotlin](http://www.vaadinonkotlin.eu/)
  You can find a complete CRUD example at [Vaadin-on-Kotlin vok-example-crud-sql2o](https://github.com/mvysny/vaadin-on-kotlin#example-project).

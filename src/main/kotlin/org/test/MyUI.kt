package org.test

import com.github.mvysny.karibudsl.v10.h1

import com.vaadin.flow.component.html.Div
import server.mycomponent.MyComponent
import com.vaadin.mpr.LegacyWrapper
import com.vaadin.flow.router.Route
import com.vaadin.flow.server.VaadinService
import com.vaadin.mpr.core.MprTheme
import com.vaadin.mpr.core.MprWidgetset
import com.vaadin.ui.*

class OldForm : VerticalLayout() {
    init {
        val name = TextField("Type your name here:")
        addComponent(name)
        addComponent(MyComponent())
        addComponent(Button("Click me", { e ->
            println("Thanks ${name.value}, it works!")
            addComponent(Label("Thanks ${name.value}, it works!"))
        }))
    }
}

@Route("")
@MprWidgetset("AppWidgetset")
@MprTheme("mytheme")
class AddressbookLayout : Div() {
    init {
        h1("Welcome to Vaadin 14!")
        add(LegacyWrapper(OldForm()))

        println("Running in production mode: Vaadin 14: ${VaadinService.getCurrent().deploymentConfiguration.isProductionMode}, Vaadin 7: ${com.vaadin.server.VaadinService.getCurrent().deploymentConfiguration.isProductionMode}")
    }
}
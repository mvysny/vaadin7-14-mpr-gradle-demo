import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.gradle.api.tasks.testing.logging.TestExceptionFormat

plugins {
    kotlin("jvm") version "1.9.21"
    // need to use Gretty here because of https://github.com/johndevs/gradle-vaadin-plugin/issues/317
    id("org.gretty") version "3.0.6"
    // you need two Vaadin plugins to build this project correctly:
    // This plugin builds the Vaadin 7 part (the widgetset/GWT)
    id("com.devsoap.plugin.vaadin") version "2.0.0.beta2"
    // This plugin builds the Vaadin 14 part (npm).
    id("com.vaadin") version "0.14.3.7"
}

defaultTasks("clean", "build")

configurations {
    compile.isCanBeResolved = true // workaround for Vaadin 8 plugin
    runtime.isCanBeResolved = true // workaround for Vaadin 8 plugin
}

repositories {
    mavenCentral()
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
}
tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

// this block configures the Vaadin 8/7 Vaadin plugin (com.devsoap.plugin.vaadin)
vaadin {
    version = "7.7.17"
}

gretty {
    contextPath = "/"
    servletContainer = "jetty9.4"
}

tasks.withType<Test> {
    useJUnitPlatform()
    testLogging {
        // to see the exceptions of failed tests in Travis-CI console.
        exceptionFormat = TestExceptionFormat.FULL
    }
}

dependencies {
    // don't use "api" or "implementation" dependencies otherwise the WAR
    // file will not contain vaadin jars.

    // Karibu-DSL dependency
    // there's no Karibu-DSL for Vaadin 7...
//    compile("com.github.mvysny.karibudsl:karibu-dsl-v8:1.0.2")
    compile("com.github.mvysny.karibudsl:karibu-dsl:1.0.2")

    // MPR + Vaadin 14 dependency
    compile("com.vaadin:vaadin-core:14.3.7") {
        // Webjars are only needed when running in Vaadin 13 compatibility mode
        listOf("com.vaadin.webjar", "org.webjars.bowergithub.insites",
            "org.webjars.bowergithub.polymer", "org.webjars.bowergithub.polymerelements",
            "org.webjars.bowergithub.vaadin", "org.webjars.bowergithub.webcomponents")
                .forEach { group -> exclude(group = group) }
    }
    compile("com.vaadin:mpr-v7:2.0.5")

    // workaround for https://github.com/johndevs/gradle-vaadin-plugin/issues/536
    vaadinCompile("com.vaadin:mpr-core:2.0.5") {
        // removes duplicate dependencies: https://gitlab.com/mvysny/vaadin14-mpr-gradle-demo/issues/1
        exclude(group = "com.vaadin")
        exclude(group = "commons-io")
    }

    // include proper kotlin version
    compile(kotlin("stdlib-jdk8"))

    // logging
    // currently we are logging through the SLF4J API to SLF4J-Simple. See src/main/resources/simplelogger.properties file for the logger configuration
    compile("org.slf4j:slf4j-simple:2.0.13")
    // this will allow us to configure Vaadin to log to SLF4J
    compile("org.slf4j:jul-to-slf4j:2.0.13")

    // test support
    // there's no Karibu-Testing for Vaadin 7...
//    testImplementation("com.github.mvysny.kaributesting:karibu-testing-v8:1.1.26")
//    testImplementation("com.github.mvysny.kaributesting:karibu-testing-v10:1.1.26")
    testImplementation("org.junit.jupiter:junit-jupiter-engine:5.11.0")

    // workaround until https://youtrack.jetbrains.com/issue/IDEA-178071 is fixed
    compile("com.vaadin:vaadin-themes:${vaadin.version}")
    compile("com.vaadin:vaadin-server:${vaadin.version}")
}

// This block configures the Vaadin 14 plugin.
// Typically Vaadin 14 plugin is configured via a vaadin{} block, but that would
// conflict with Vaadin 7 plugin (which is also configured via a vaadin{} block).
// In such cases, Vaadin 14 plugin avoids this kind of conflicts by allowing the configuration
// via a vaadin14{} block.
vaadin14 {
    pnpmEnable = true
}

afterEvaluate {
    if (vaadin14.productionMode) {
        dependencies {
            // this also enables production mode for Vaadin 7. See
            // https://gitlab.com/mvysny/vaadin7-14-mpr-gradle-demo/-/issues/1
            // for more details.
            compile("com.vaadin:flow-server-production-mode:2.3.5")
        }
    }
}
if (JavaVersion.current() > JavaVersion.VERSION_1_8) {
    logger.error("Vaadin 7 widgetset compilation requires Java 8 - it won't work on Java 11+. Current Java: ${JavaVersion.current()}")
}
if (JavaVersion.current() > JavaVersion.VERSION_11) {
    throw GradleException("Vaadin 7 widgetset compilation requires Java 8 - it won't work on Java 11+. Current Java: ${JavaVersion.current()}")
}
